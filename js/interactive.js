var margin = {top: 30, right: 20, bottom: 30, left: 60},
	width  = 960 - margin.left - margin.right,
	height = 600 - margin.top  - margin.bottom;

var chart = d3.select("#chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var scaleX = d3.scale.log()
    .domain([2000000, 3487872233])
    .range([0, width]);

var scaleY = d3.scale.log()
    .domain([9, 1600])
    .range([height,0]);

/*var scaleX = d3.time.scale()
    .domain([new Date('2010-01-01'), new Date('2013-01-01')])
    .rangeRound([0, width]);*/

var radius = d3.scale.linear()
    .domain([0, 3487872233])
    .range([5, width/20]);

var xAxis = d3.svg.axis()
    .scale(scaleX)
    .orient('bottom')
    .ticks(6)
    .tickFormat(function(d) { return "$"+(d/1000000)+"m"; })
    //.tickFormat(d3.time.format('%Y'))
    .tickSize(0)
    .tickPadding(8);

var yAxis = d3.svg.axis()
    .scale(scaleY)
    .orient('left')
    .ticks(1)
    .tickSize(5)
    .tickFormat(function(d) { return d + "%"; })
    .tickPadding(8);

chart.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0, ' + (height) + ')')
    .call(xAxis);

chart.append('g')
  .attr('class', 'y axis')
  .call(yAxis);

d3.csv("data/inc500ranking.csv", function(data) {

    /*data.sort(function (a, b) {
      if (+a.Rank < +b.Rank) {
        return 1;
      }
      if (+a.Rank > +b.Rank) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });*/

	var circles = chart.selectAll("circle")
		.data(data, function(d){return d.Rank})
	  .enter().append("circle")
	  	.attr("class", "companies")
	  	.attr("r", 5)
	  	.attr("cx", 0)
      .attr("cy", function(d,i) { return i; /*scaleY(d["Verified Revenue Growth Rate 2014"]/100)*/});

	circles.on("click", function(){
		circles.transition()
      .duration(function(d) { return 100000000/d["Verified Revenue Growth Rate 2014"];})
      //.delay(function(d, i) { return i * 50; })
      .ease("linear")
      .attr("cx", function(d) { return width; /*scaleX(d["Verified Revenue For 2013"])*/})
      .attr("r", function(d) { return scaleX(d["Verified Revenue For 2013"])/50; });
      
	})
	

	

});